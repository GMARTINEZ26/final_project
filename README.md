## Final project for AOS573:
## Plotting soundings from the 2006 AMMA field campaign.

# Description

This notebook expands on a section of my ongoing research project, [CPEX-CV](https://espo.nasa.gov/cpex-cv/content/CPEX-CV) and my thermodynamics research paper. I looked into learning about African Easterly Waves (AEWs) and their influence on mesoscale convective systems. The following notebook references research studies that observed squall lines that progressed in West Africa and followed a path from continental to coastal to oceanic sites to see if there was any influence from AEWs. Also, I provided information about African Easterly Jets (AEJs) as it will offer context about the 2006 West African monsoon season.

This notebook plots West Africa and marked continental (Niamey, Niger), coastal (Dakar, Senegal) and oceanic (Praia, Cape Verde) sites. Also plotting AEW location and AEJ vacillation for 01 September 2006 to 04 September 2006. Finally, real dropsonde data from the [AMMA field campaign](https://www.eol.ucar.edu/field_projects/amma) that took place in August 2006 to observe any unique characteristics that may move our research question forward.

# Getting Started

`Please make sure you don't overwrite the data file (upaqf_08596) in the repository otherwise plots will not work!`

- [ ] Begin by using our AOS573 environment.
- [ ] All packages used (Matplotlib, Metpy, Pandas, Cartopy, etc.) are in our AOS573 environment.

## In the final_project folder

- [ ] upaqf_08596: The data file is derived from [EOL Database website](https://data.eol.ucar.edu/dataset/500.002).
- [ ] PraiaWFF.jpg is an image file derived from Cifelli et al. 2010 depicting the (a) V wind component, (b) U wind component, (c) relative humidity,  and (d) CAPE values over Praia, Cape Verde from 02 September 2006 to 05 September 2006 for every 12 hours.
- [ ] Westafrica.png is an image file derived from Adedoyin, 1988 that illustates northeastern trade winds, monsoon front and the interaction of a squall line (SL), AEW (thin lines) and AEJ.

- [ ] final_env.yml: Environment file (the AOS573 environment will work on this repository).
- [ ] Final_573: The code!



